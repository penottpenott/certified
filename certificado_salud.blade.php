<!DOCTYPE html>
<html lang="en">
<head>
	<title>FUNDASALUD | Certificado de Salud</title> 
	<!-- For-Mobile-Apps-and-Meta-Tags -->
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="Cookhouse Responsive, Login Form Web Template, Flat Pricing Tables, Flat Drop-Downs, Sign-Up Web Templates, Flat Web Templates, Login Sign-up Responsive Web Template, SmartPhone Compatible Web Template, Free Web Designs for Nokia, Samsung, LG, Sony Ericsson, Motorola Web Design" />
	<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
	<!-- //For-Mobile-Apps-and-Meta-Tags -->
	<!-- Custom Theme files -->
	<link href="{{ asset('certifi/css/bootstrap.css') }}" type="text/css" rel="stylesheet" media="all">
	<link href="{{ asset('certifi/css/style.css') }}" type="text/css" rel="stylesheet" media="all"> 
	<!-- //Custom Theme files -->
	<!-- font-awesome-icons -->
	<link rel="stylesheet" href="{{ asset('certifi/css/font-awesome.min.css') }}" />
	<!-- //font-awesome-icons -->
	<!-- js -->
	<script type='text/javascript' src="{{ asset('certifi/js/jquery-2.2.3.min.js') }}"></script>
<!-- //js -->
</head>

<body class="bg">
<!-- banner1 -->
	<div>
		<img src="{{ asset('certifi/images/logo_pie_sw.jpg') }}" alt=" " class="img-responsive wthree_velit"/>
	</div>
<!-- //banner1 -->

<!-- single -->
	<div class="menu">
		<div class="container">	
			<div class="agileinfo_team_grids">
				
			</div>
			<div class="grid_3 grid_5">
				<div align="center">
					<h3>CERTIFICADO DE SALUD</h3>
				</div>
				<div class="col-md-6">
					<div class="agileinfo_team_grid">
						<img src="{{ asset('certifi/images/user.jpg') }}" alt=" " class="img-responsive wthree_velit" border="1" />
						<div class="clearfix"></div>
					</div>
				</div>
				<div class="col-md-6">
					<table class="table table-bordered">
						<tbody>
							<tr>
								<td>NOMBRES</td>
								<td>Victor Luis</td>
							</tr>
							<tr>
								<td>APELLIDOS</td>
								<td>Briceño Mejias</td>
							</tr>
							<tr>
								<td>CÉDULA </td>
								<td>17878724</td>
							</tr>
							<tr>
								<td>EXPEDICIÓN</td>
								<td>2020-10-10</td>
							</tr>
							<tr>
								<td>VENCIMIENTO</td>
								<td>2021-10-10</td>
							</tr>

							<tr>
								<td>STATUS</td>
								<td><span class="badge badge-primary">Vigente</span></td>
							</tr>
							<tr>
								<td>SERIAL</td>
								<td>IUOIU9889</td>
							</tr>
							
						</tbody>
					</table>                    
				</div>
				
			</div>	
		</div>
	</div>
	
<!-- single -->
<!-- footer -->
	<div class="footer">
		<div class="container">
			<div class="w3_agile_copyright">	
				<p>&copy; Copyright <script>document.write(new Date().getFullYear());</script>. Todos los Derechos Reservados. Fundasalud | Coord. Regional de Informática y Telecomunicaciones. </p>
			</div>
		</div>
	</div>
<!-- //footer -->
</body>
</html>	